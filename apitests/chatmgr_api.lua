--[[
    (C) Copyright 2015, Recode Systems UG & Co. KG
    All rights reserved.
 ]]
-------------------------------------------------------------------------------
-- This is a script to automatically test a few of our APIs:
-- ChatMgr
-- Preparation:
--   - start script
--   - travel to Hall of Monuments
-------------------------------------------------------------------------------

include "static_test_data.lua"
include "misc.lua"

-------------------------------------------------------------------------------
-- ChatMgr functions
-------------------------------------------------------------------------------

msg_received = false

function Test_ChatMgr()
    assert(msg_received, "chat message not received")
end

-------------------------------------------------------------------------------

function OnWorldReveal()
    msg_received = false

    -- check if we are on the right map
    mapid = Client:GetMapId()
    if (mapid ~= HOM_MAPID) then
        print("ERROR: Wrong map. Travel to Hall of Monuments.")
        return
    end

    MaxTestDuration(10000)
    Client:GetChatMgr():SendMessage(ChatMgr.Channel.Map, TEST_MESSAGE)
end

function OnChatMessageReceived(channel, sender, msg)
    if (msg == TEST_MESSAGE and channel == ChatMgr.Channel.Map) then
        msg_received = true
    end

    Test_ChatMgr()
    assert(false, "TESTS DONE")
end

function init()
    print("############################################################")
    print("# API test script initialized")
    print("# This script will test all ChatMgr functions")
    print("# To start this script, travel to Hall of Monuments")
    print("############################################################")

    chatMgr = Client:GetChatMgr():RegisterTrigger(ChatMgr.OnChatMessageReceived, OnChatMessageReceived);
    Client:RegisterTrigger(Gw2Client.OnWorldReveal, OnWorldReveal)
end