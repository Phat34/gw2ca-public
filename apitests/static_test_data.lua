--[[
    (C) Copyright 2015, Recode Systems UG & Co. KG
    All rights reserved.
 ]]
-------------------------------------------------------------------------------
-- This file contains static data for our tests.
-- If something changes here, we will have problems with our tests. So we have
-- to keep everything in this file up to date.
-------------------------------------------------------------------------------

-- Hall of Monuments map id
HOM_MAPID            = 807

-- HOM Map data
OWN_ID               = 9 -- own agent id in Hall of Monuments
HOM_AGENT_COUNT      = 6 -- number of agents in Hall of Monuments (near start position)

-- Artificer Mullenix agent data
MULLENIX_ID          = 13
MULLENIX_NAME        = "Artificer Mullenix"
MULLENIX_POS         = WorldPos(-4227, 2409, -74)
MULLENIX_SPECIESID   = 536881742

-- Kimmes the Historian agent data
KIMMES_POS           = WorldPos(-8949, 7244, -2)

-- Xunlai Chest 1
XUNLAI_CHEST1_ID     = 1
XUNLAI_CHEST1_POS    = WorldPos(-1547, 3116, -76)

-- Xunlai Chest 2
XUNLAI_CHEST2_ID     = 2

-- Test position
-- if you move to this position, the OnAgentDespawned event will be called twice
TEST_POS             = WorldPos(-7041, 5083, -201)

-- Mullenix dialog data
MULLENIX_DIALOG_OPT_COUNT        = 2    -- first page
MULLENIX_DIALOG_FIRST_PAGE_ID    = 0

-- Waypoint data
WP_DATA =
{
    -- [map id      , number of waypoints]
    {mapid = 18     , nwps = 13},        -- Divinity's Reach, 13 Waypoints
    {mapid = 218    , nwps = 12},        -- Black Citadel, 12 Waypoints
    {mapid = 326    , nwps = 14},        -- Hoelbrak, 12 Waypoints
    {mapid = 139    , nwps = 9},         -- Rata Sum
}

-- test message for our chat api test
TEST_MESSAGE = "thisisatestmessage"

-- Heart of the Mists
HOTM_MAPID           = 350
FIRE_POS             = WorldPos(2799, 1432, -1835)
-- DPS Trials Waypoint id
DPS_TRIALS_WPID      = 1354
-- PvP Lobby Waypoint
PVP_LOBBY_WPID       = 1046
-- Target Golem (Indestructible)
GOLEM_POS            = WorldPos(-5740, 1609, -1538)

-- Skill ids
SOR_SKILLID          = 312  -- Signet of Resolve skill id
SHELTER_SKILLID      = 259  -- Shelter skill id

-- Trait ids
FORCE_OF_WILL        = 1682
WRATHFUL_SPIRIT      = 563