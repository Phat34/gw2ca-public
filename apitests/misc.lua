--[[
    (C) Copyright 2015, Recode Systems UG & Co. KG
    All rights reserved.
 ]]
 
function distance(p1, p2)
    local dx = p1.x - p2.x
    local dy = p1.y - p2.y
    return math.sqrt(dx * dx + dy * dy)
end

function DelayedTest(test, ms)
    local tmr = Client:GetTimer()
    tmr:RegisterTrigger(test, ms, 0)
end

function test_failed()
    assert(false, "TEST FAILED: Maximal test duration exceeded")
end

function MaxTestDuration(ms)
    DelayedTest(test_failed, ms)
end