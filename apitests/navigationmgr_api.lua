--[[
    (C) Copyright 2015, Recode Systems UG & Co. KG
    All rights reserved.
 ]]
-------------------------------------------------------------------------------
-- This is a script to automatically test a few of our APIs:
-- NavigationMgr
-- Preparation:
--   - start script
--   - travel to Hall of Monuments
-------------------------------------------------------------------------------

include "static_test_data.lua"
include "misc.lua"

-------------------------------------------------------------------------------
-- NavigationMgr functions
-------------------------------------------------------------------------------
OnDisplayDialog_called = false
function OnDisplayDialog(dialog)
    assert(dialog, "dialog is nil")
    OnDisplayDialog_called = true
end

function Prepare_Test_NavigationMgr_Teleport()
    local dlgMgr = Client:GetDialogMgr()
    dlgMgr:RegisterTrigger(DialogMgr.OnDisplayDialog, OnDisplayDialog)

    local navMgr = Client:GetNavigationMgr()
    navMgr:Teleport(MULLENIX_POS)
end

function WaitDialog()
    assert(OnDisplayDialog_called, "OnDisplayDialog was not called")
    assert(false, "TESTS DONE")
end

function Test_NavigationMgr_Teleport()
    local agent = Client:GetAgentMgr():GetOwnAgent()
    assert(agent, "GetOwnAgent() failed")

    local pos = agent:GetPosition()
    assert( distance(pos, MULLENIX_POS) < 1.0, "Test_NavigationMgr_Teleport failed")

    local cchar = Client:GetControlledCharacter()
    local agentMgr = Client:GetAgentMgr()
    local tmr = Client:GetTimer()

    -- Artificer Mullenix
    local agent = agentMgr:GetAgentByName(MULLENIX_NAME)
    assert(agent, "agentMgr:GetAgentByName failed")

    -- try to interact with mullenix
    cchar:Interact(agent)
    DelayedTest(WaitDialog, 3000)
end

function OnWorldReveal()
    -- check if we are on the right map
    local mapid = Client:GetMapId()
    if (mapid ~= HOM_MAPID) then
        error("ERROR: Wrong map. Travel to Hall of Monuments.")
        return
    end

    MaxTestDuration(10000)
    Prepare_Test_NavigationMgr_Teleport()
    DelayedTest(Test_NavigationMgr_Teleport, 2000)
end

function init()
    debug("############################################################")
    debug("# API test script initialized")
    debug("# Testing NavigationMgr")
    debug("############################################################")

    Client:RegisterTrigger(Gw2Client.OnWorldReveal, OnWorldReveal)
end