--[[
This utility script can be used to populate/update the MapTable object residing in the maps.lua module.

Usage:
If entering an unknown map call SetName() with a name for the variable which can be a abbreviation and the full name of the map as second argument.
This has to be done one time. All subsequent calls to SetName() will rename the map instead.

To capture transitions between maps (portals), call Capture() to active capture mode and leave the map through a portal.
Be careful to not run straight into a portal but to slowly approach it in order to record a accurate position (lags can cause you to be teleported much later).
Make sure that you capture the transition in the opposite direction as well.
IMPORTANT: Don't use waypoints (or any other form of inter-map teleports) while capturing. This will produce a false transition.
Capture mode will stay active until deactivated by Cancel(). 

To save your results call PrintCode() and copy the produced string to util.lua. Be careful, the code is split into multiple log messages,
make sure that you remove all timestamps (search for '[')
]]

local m_util = include("gw2/util.lua")
local m_maps = include("gw2/maps.lua")
local m_colors = include("gw2/colors.lua")

-- maps MapId to variable name
local names = {}
for key, obj in pairs(m_maps) do
	if m_util.Inherits(obj, m_maps.Map) then
		names[obj.id] = key
	end
end

local drawObjs = {}

-- expose maps for manual editing:
mapTable = m_maps

-- Gets and stores all waypoints of the current map in the associated Map object
local function CaptureWaypoints()
    local map = m_maps:GetCurrentMap()
    if map then
        map.waypoints = {}
        for poi in Client:GetPoiMgr():GetWaypoints() do
            map.waypoints[poi.id] = m_maps.Waypoint:new(poi.id, map.id, poi.pos)
        end
    end
end

-- Prints out basic informations about the current map
local function WorldRevealHandler()
	local mapId = Client:GetMapId()
	local pos = m_util.GetPosition()
	
	if not m_maps:HasMap(mapId) then
		information("Entered unknown map " .. mapId .. "; Please specify a name using SetName()")
	else
		information("Entered " .. m_maps:GetName(mapId) .. " - " .. mapId)
        CaptureWaypoints()

        for i, trans in ipairs(m_maps:GetMap(mapId).transitions) do
            table.insert(drawObjs[Client:GetId()], Client:GetDrawMgr():DrawCircle3D(trans.startPos, 10, m_colors.Teal))
        end
	end
end

-- Prints out a notification about the leave and setups singleshot for transition capture
local function WorldLeaveHandler(isLogout)
	for i, drawObj in ipairs(drawObjs[Client:GetId()]) do
        Client:GetDrawMgr():DrawRemove(drawObj)
    end
    drawObjs[Client:GetId()] = {}
    
    if isLogout then
		debug("Logged out")
		return
	end
	
	local mapId = Client:GetMapId()
	local pos = m_util.GetPosition()
	
	if not m_maps:HasMap(mapId) then
		information("Left unknown map " .. mapId)
	else
		information("Left " .. m_maps:GetName(mapId) .. " - " .. mapId)
	end
end

-- OnWorldLeave handler, which stores the old map informations using a closure
local function StartCaptureHandler()
	local oldMapId = Client:GetMapId()
	local oldPos = m_util.GetPosition()

	if not m_maps:HasMap(oldMapId) then
		error("Can't capture transitions for unknown maps. Call SetName() once your back on the last map")
		return
	end
	
	local function TransitionHandler()
		local curMapId = Client:GetMapId()
		local curPos = m_util.GetPosition()
		
		if curMapId == mapId then
			return
		end
		
		if m_maps[oldMapId]:AddTransition(curMapId, oldPos, curPos) then
			notice("Recorded transition: " .. oldMapId .. " -> " .. curMapId)
		end
	end

	Client:RegisterSingleshot(Gw2Client.OnWorldReveal, TransitionHandler)
end

-- Generates the code containing the static data
function PrintCode()
	local str = {}
	
	local integrity = m_maps:CheckIntegrity()
	if integrity ~= nil then
		error("Maps are not valid: " .. integrity)
		return
	end
	
	-- print ctors and transitions:
	for id, map in m_util.orderedPairs(m_maps:GetAll()) do
		
		local varName = names[map.id]
		if(varName == nil) then
			error("Map without a variable name: " .. map.id)
			return
		end
		
		-- prepend "var."
		varName = "vars." .. varName
		
		local levelStr = ""
		if map.minLevel ~= 0 then
			levelStr = ", " .. map.minLevel
		end
		
		table.insert(str, varName .. " = Map:new(" .. map.id .. ", \"" .. map.name .. "\"" .. levelStr .. ")")
		
        -- print transitions
		for i, trans in ipairs(map.transitions) do
			table.insert(str, varName .. ":AddTransition(" .. trans.toId .. ", " .. tostring(trans.startPos) .. ", " .. tostring(trans.endPos) .. ")")
		end
		
        -- print waypoints
        for wpId, wp in pairs(map:GetWaypoints()) do
            table.insert(str, varName .. ":AddWaypoint(" .. wpId .. ", " .. tostring(wp.pos) .. ")")
        end
        
		table.insert(str, "") -- extra newline
	end
	
	table.insert(str, "") -- extra newline
	
	-- print maps:AddMap(...) statements:
	for id, map in m_util.orderedPairs(m_maps:GetAll()) do
		local varName = "vars." .. names[map.id] -- dont have to check anymore
		table.insert(str, "mapTable:AddMap(" .. varName .. ")")
	end
	
	m_util.LogLongMessage(notice, table.concat(str, "\n"))
end

-- Sets the name of a map,
-- this function MUST be called initially on unknown maps to "register" them
function SetName(varName, displName)
	local mapId = Client:GetMapId()
	
	if m_maps:HasMap(mapId) then
		if names[mapId] == nil then
			critical("Integrity compromised: Map without variable name!")
			return
		end
		
		-- check if there is already another map with this variable name:
		for id, name in pairs(names) do
			if name == varName and id ~= mapID then
				error("Variable name \"" .. varName .. "\" already used for map " .. id .. " \"" .. m_maps:GetName(id) .. "\"")
				return
			end
		end
		
		local renamed = false
		
		-- new variable name?
		if varName ~= names[mapId] then
			names[mapId] = varName
			information("Changed variable name for map " .. mapId .. " to \"" .. varName .. "\"")
			
			renamed = true
		end
	
		-- new display name?
		local oldName = m_maps[mapId].name
		if oldName ~= displName then
			m_maps[mapId].name = displName
			information("Renamed map " .. mapId .. " from \"" .. oldName .. "\" to \"" .. displName .. "\"")
			
			renamed = true
		end
		
		if not renamed then
			warning("Nothing changed")
		end
	else
		-- First call: Add a new map
		m_maps:AddMap(m_maps.Map:new(mapId, displName))
		names[mapId] = varName
        CaptureWaypoints()
		information("Registered " .. displName .. " (id: " .. mapId .. ")")
	end
end

-- Starts transition capture
function Capture()
	Client:RegisterTrigger(Gw2Client.OnWorldLeave, StartCaptureHandler)
	information("Capturing transitions, please leave map through portal")
end

-- Cancels transition capture
function Cancel()
	Client:RemoveTrigger(Gw2Client.OnWorldLeave, StartCaptureHandler)
	information("Cancelled transition capture")
end

--[[ 
Teleports to the closest transition.
Use this function to test whether a transition has been properly captured.
]]
function TestTransition()
    local curMap = m_maps:GetCurrentMap()
    local curPos = m_util.GetPosition()
    
    if curMap == nil then
        error("The current map isn't registered and does not have any transitions")
        return
    end
    
    local closest = nil
    for i, trans in ipairs(curMap.transitions) do
        if closest == nil then
            closest = trans
        elseif m_util.ComputeDistance(curPos, trans.startPos) < m_util.ComputeDistance(curPos, closest.startPos) then
            closest = trans
        end
    end
    
    if closest == nil then
        error("There are no transitions on this map")
        return
    end
    
    information("Testing: " .. curMap.id .. " -> " .. closest.toId .. "  (" .. curMap.name .. " -> " .. m_maps:GetName(closest.toId) .. ")")
    
    Client:GetNavigationMgr():Teleport(closest.startPos)
end

local function OnDisconnect()
	drawObjects[Client:GetId()] = nil
    
    notice("Client disconnected")
end

function init()
    notice("Client attached")

    drawObjs[Client:GetId()] = {}
    
	Client:RegisterTrigger(Gw2Client.OnWorldReveal, WorldRevealHandler)
	Client:RegisterTrigger(Gw2Client.OnWorldLeave, WorldLeaveHandler)
	Client:RegisterTrigger(Gw2Client.OnDisconnect, OnDisconnect)
end

notice("MapLogger started")
