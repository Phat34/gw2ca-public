notice("Testing util.lua module")
include("util_test.lua")
notice("[Passed]")

notice("Testing rand.lua module")
include("rand_test.lua")
notice("[Passed]")

notice("Testing fsm.lua module")
include("fsm_test.lua")
notice("[Passed]")

notice("Testing Money class")
include("money_test.lua")
notice("[Passed]")

notice("Everything passed\n")

exit() -- will throw an error and terminate the "bot" ;)