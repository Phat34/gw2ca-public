local m_fsm = include("gw2/fsm.lua")

local function test_simple()
    information("Testing simple fsm")

	local A = m_fsm.State:new("a")
	local B = m_fsm.State:new("b")
	local sm = m_fsm.StateMachine:new({A, B}, A, {
				{A, "toB", B},
				{B, "toA", A}
			})
			
	sm:Start()
	assert(sm.current == A, "StateMachine:Start() not working (wrong state)")
    
    sm:Fire("toB")
	assert(sm.current == B, "StateMachine:Fire() not working (wrong state)")

	sm:Fire("toA")
	assert(sm.current == A, "StateMachine:Fire() not working (wrong state)")
    
    information("Passed")
end

local function test_direct_transition()
    information("Testing direct transitions")

    local A = m_fsm.State:new("a")
    local B = m_fsm.State:new("b")
    local C = m_fsm.State:new("c")
    local D = m_fsm.State:new("d")
    
    local fsm = m_fsm.StateMachine:new({A, B, C, D}, A, {
                {A, nil, B},
                {B, "toC", C},
                {C, nil, D}
            })
    
    fsm:Start()
    assert(fsm.current == B, "Direct transition not working when calling Start() (wrong state)")

    fsm:Fire("toC")
    assert(fsm.current == D, "Direct transition not working when calling Fire() (wrong state)")
    
    information("Passed")
end

local function test_guards()
    information("Testing guards")
    
    local i = 0
    
    local A = m_fsm.State:new("a")
	local B = m_fsm.State:new("b")
    local C = m_fsm.State:new("c")
    local D = m_fsm.State:new("D")
	local fsm = m_fsm.StateMachine:new({A, B, C, D}, A, {
				{A, "event", B, condition = function() return i == 1 end},
                
                {B, "event", C, condition = function() return true end},
                {B, "event", B, condition = function() return true end},
                
                {C, "event", D, condition = function() return true end},
                {C, "event", C},
                
                {D, "event", D, condition = function() return false end},
                {D, "event", A}
			})
            
    fsm:Start()
    
    fsm:Fire("event")
    assert(fsm.current == A, "Basic guard not working")
    
    i = 1
    fsm:Fire("event")
    assert(fsm.current == B, "Basic guard not working")
    
    fsm:Fire("event")
    assert(fsm.current == C, "Guard precedence not working")
    
    fsm:Fire("event")
    assert(fsm.current == D, "else transitions not working (takes precedence over passed guards)")
    
    fsm:Fire("event")
    assert(fsm.current == A, "else transitions not working (not executing)")
    
    information("Passed")
end

local function test_actions()
    information("Testing actions")
    
    local i = 0
    
    local A = m_fsm.State:new("a")
    local B = m_fsm.State:new("b")
    local C = m_fsm.State:new("c")
    
    local fsm = m_fsm.StateMachine:new({A, B, C}, A, {
                {A, nil, B, function() i = 1 end},
                {B, "toC", C, function() return "foo", "bar" end}
            })
    
    fsm:Start()
    assert(i == 1, "actions not firing")
    
    function C:OnEnter(arg1, arg2) 
        assert(arg1 == "foo" and arg2 == "bar", "action return values are not forwarded to OnEnter()") 
    end
    fsm:Fire("toC")
    
    information("Passed")
end

local function test_submachine()
    information("Testing composite submachines")
    
	local i = 0
	local saa = m_fsm.State:new("a")
	local sab = m_fsm.State:new("b")
	
	function sab:OnEnter()
		i = i+1
	end
	
	function saa:OnEnter()
		i = i+1
	end
	
	local asm = m_fsm.StateMachine:new({saa, sab}, saa, {
				{saa, "tob", sab},
				{sab, "toa", saa}
			})
	
	local sa = m_fsm.State:new("a", asm)
	local sb = m_fsm.State:new("b")
	local sm = m_fsm.StateMachine:new({sa, sb}, sa, {
				{sa, "tob", sb},
				{sb, "toa", sa},
				{sb, "toab", sab}
			})
			
	sm:Start()
	assert(sm.current == sa, "StateMachine:Start() not working (wrong state)")
	assert(sa.submachine.current == saa, "StateMachine:Start() not working (wrong submachine state)")
	
	sm:Fire("tob")
	assert(sm.current == sa, "StateMachine:Fire() not working (wrong parent state)")
	assert(sa.submachine.current == sab, "StateMachine:Fire() not working (wrong submachine state)")
	
	sm:Fire("tob")
	assert(sm.current == sb, "StateMachine:Fire() not working (wrong state)")
	
	sm:Fire("toab")
	assert(sm.current == sa, "StateMachine:Fire() not working (wrong state)")
	assert(sa.submachine.current == sab, "StateMachine:Fire() not working (wrong state)")
	assert(i == 3, "StateMachine:Fire() not working (event handler not properly executed)")
    
    information("Passed")
end

test_simple()
test_direct_transition()
test_guards()
test_actions()
test_submachine()
