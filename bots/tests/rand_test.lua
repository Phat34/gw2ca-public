local m_rand = include("gw2/rand.lua")

local function LowerRNG()
    return 0
end

local function UpperRNG()
    return 1
end

local function TieRNG()
    return 0.5
end

assert(m_rand.Round(0.1) == 0, "Round() doesn't properly round to lower")
assert(m_rand.Round(0.9) == 1, "Round() doesn't properly round to upper")
assert(m_rand.Round(0.5) == 0, "Round() doesn't properly round to even")
assert(m_rand.Round(1.5) == 2, "Round() doesn't properly round to even")

assert(m_rand.Random(nil, nil, UpperRNG) == 1, "Random() failed")

assert(m_rand.Random(42, nil, LowerRNG) == 1, "Random(upper) failed")
assert(m_rand.Random(42, nil, UpperRNG) == 42, "Random(upper) failed")
assert(m_rand.Random(42, nil, TieRNG) == 22, "Random(upper) failed")

assert(m_rand.Random(21, 42, LowerRNG) == 21, "Random(lower, upper) failed")
assert(m_rand.Random(21, 42, UpperRNG) == 42, "Random(lower, upper) failed")
assert(m_rand.Random(21, 42, TieRNG) == 32, "Random(lower, upper) failed")

assert(m_rand.RandomFloat(0, 5, TieRNG) == 2.5, "RandomFloat failed")

assert(m_rand.RandDistribute(10, 0.1, LowerRNG) == 9, "RndDistribute() failed")
assert(m_rand.RandDistribute(10, 0.1, UpperRNG) == 11, "RndDistribute() failed")

assert(m_rand.RandDistributeAbs(10, 2, LowerRNG) == 8, "RndDistributeAbs() failed")
assert(m_rand.RandDistributeAbs(10, 2, UpperRNG) == 12, "RndDistributeAbs() failed")

local toShuffle = {1, 2, 3}
local copyShuffled = m_rand.Shuffle(toShuffle, LowerRNG)
local inplaceShuffled = m_rand.ShuffleInplace(toShuffle, LowerRNG)
assert(copyShuffled[1] == 3 and copyShuffled[2] == 1 and copyShuffled[3] == 2,"Shuffle() failed")
assert(inplaceShuffled[1] == 2 and inplaceShuffled[2] == 3 and inplaceShuffled[3] == 1, "ShuffleInplace() failed")

