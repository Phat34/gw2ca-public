--[[
String and log related utility functions
]]



--[[
Given a number returns a string with the representation of the number in the given base.
base must be an integer between 2 and 36
]]
local itoaLockup = {"0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z"}
local function itoa(number, base)
    assert(type(number) == "number", "first arg must be a number")
    assert(base % 1 == 0 and 2 <= base and base <= 36, "base must be an integer between 2 and 36")

    local str = {}
    local remainder
    
    repeat
        remainder = number % base
        number = (number-remainder) / base
        table.insert(str, itoaLockup[remainder+1])      
    until number==0
   
    ReverseArray(str)
    return table.concat(str)
end

--[[
Pads strings to the given length by prepending the given char.
If no char is given a whitespace character is used.
]]
local function LPad(str, len, char)
    char = char or " "
    
    assert(type(str) == "string", "`str` must be a string")
    assert(type(len) == "number" and len >= 0 and len % 1 == 0, "`len` must be a positive integer")
    assert(type(char) == "string" and #char == 1, "`char` must be a single character")
    
    return string.rep(char, len - #str) .. str
end

--[[
Pads strings to the given length by appending the given char.
If no char is given a whitespace character is used.
]]
local function RPad(str, len, char)
    char = char or " "
    
    assert(type(str) == "string", "`str` must be a string")
    assert(type(len) == "number" and len >= 0 and len % 1 == 0, "`len` must be a positive integer")
    assert(type(char) == "string" and #char == 1, "`char` must be a single character")
    
    return str .. string.rep(char, len - #str)
end

local Table = {}
Table.__index = Table

function Table:new(...)
    local obj = setmetatable({}, self)
    obj.__index = obj
    
    obj.headers = {...}
    for i, header in ipairs(obj.headers) do
        assert(type(header) == "string", "Column headers must be strings")
    end
    assert(#obj.headers > 0, "A table needs at least one column, pass \"\" (empty string) if you don't want to display a header")
    
    obj.rows = {}
    
    return obj
end

function Table:AddRow(...)
    local args = {...}
    
    assert(#args <= #self.headers, "A row must not contain more cells than columns")
    
    local row = {}
    for i=1, #self.headers do -- we don't use ipairs here, so that nil args are properly included
        local val = args[i]
        if val ~= nil then
            table.insert(row, tostring(val))
        else
            table.insert(row, "")
        end
    end
    
    table.insert(self.rows, row)
end

function Table:render(width, padding)
    padding = padding or 2
    
    assert(width == nil or (type(width) == "number" and width >= 0 and width % 1 == 0), "`width` must be a positive integer")
    assert(type(padding) == "number" and padding >= 0 and padding % 1 == 0, "`padding` must be a positive integer")
    
    local nCol = #self.headers
    
    -- calculate required space for each column to fully display every value:
    local reqWidths = {}
    for i, header in ipairs(self.headers) do
        reqWidths[i] = string.len(header)
    end
    
    for i, row in ipairs(self.rows) do
        for j, str in ipairs(row) do
            local length = string.len(str)
            if length > reqWidths[j] then
                reqWidths[j] = length
            end
        end
    end
    
    local colWidths = reqWidths
    
    -- fixed width:
    if width then
        local usableWidth = width - nCol*(padding + 1) - 1
        local avgWidth = math.floor(usableWidth / nCol)
    
        -- calculate difference in characters:
        local dif = usableWidth
        for i, reqWidth in ipairs(reqWidths) do
            dif = dif - reqWidth
        end
        
        if dif > 0 then
            -- we got more space than required, lets distribute it evenly
            local avgDif = dif / nCol
            local rest = dif % nCol
            local fl = math.floor(avgDif)
            
            for i=1, nCol do
                if i <= rest then
                    colWidths[i] = colWidths[i] + fl + 1
                else
                    colWidths[i] = colWidths[i] + fl
                end
            end
        elseif dif < 0 then
            -- TODO: we have to cut
            assert(false, "Elision not implemented yet, make sure that you choose a big enough `width`")
        end
    end
    
    -- build table string:
    local builder = {}
    local padStr = string.rep(" ", padding)
    local LSep = "|" .. padStr
    local CSep = padStr .. "|" .. padStr
    local RSep = padStr .. "|\n"
    
    -- line:
    local line = {}
    table.insert(line, "+")
    for i = 1, nCol do
        table.insert(line, string.rep("-", colWidths[i] + 2 * padding))
        table.insert(line, "+")
    end
    line = table.concat(line)
    
    -- top frame:
    table.insert(builder, line)
    table.insert(builder, "\n")
    
    -- column headers:
    for i, header in ipairs(self.headers) do
        if i == 1 then
            table.insert(builder, LSep)
        else
            table.insert(builder, CSep)
        end
        table.insert(builder, RPad(header, colWidths[i]))
    end
    table.insert(builder, RSep)
    
    -- separator:
    table.insert(builder, line)
    table.insert(builder, "\n")
    
    -- rows:
    for i, row in ipairs(self.rows) do
        for j, val in ipairs(row) do
            if j == 1 then
                table.insert(builder, LSep)
            else
                table.insert(builder, CSep)
            end
            table.insert(builder, RPad(val, colWidths[j]))
        end
        table.insert(builder, RSep)
    end
    
    -- bot frame:
    table.insert(builder, line)
    
    return table.concat(builder)
end

local module = {
    Table = Table,
    itoa = itoa,
    LPad = LPad,
    RPad = RPad,
}

return module