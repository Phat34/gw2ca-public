local module = {}

--[[
-- Time units
-- All return values are based on millisecond timers
-- - ms
-- - sec
-- - min
-- - h
--]]

function module.ms(val)
    return val
end

function module.sec(val)
    return val * 1000
end

function module.min(val)
    return module.sec(val) * 60
end

function module.h(val)
    return module.min(val) * 60
end

function module.d(val)
    return module.h(val) * 24
end

return module;