--[[
Represents a module.
Modules that have the same name export the same interface
and thus can be used interchangeably
]]
local Module = {}
Module.__index = Module

--[[
Creates a new Module. Should be called in the file defining the Module and then returned
@param name the name of the module
@param t a table defining the interface of the module
]]
function Module:new(name, t)
    local obj = setmetatable({}, self)
    obj.name = name
    obj.content = t
    return obj
end

--[[
Can be overridden to perform per module initialization.
]]
function Module:Init()
    information("Loaded module " .. name)
end

--[[
Can be overridden to perform per module deinitialization
]]
function Module:DeInit()
    information("Unloaded module " .. name)
end

------------------------------------------

--[[
The registry holding all loaded modules
]]
local ModuleRegistry = {}
ModuleRegistry.__index = ModuleRegistry

--[[
Returns the module specified by the given name or nil if no such module is loaded
]]
function ModuleRegistry:GetModule(name)
    if not self.modules[name] then return nil end
    return self.modules[name].content
end

--[[
Loads the given module
@param file the file containing the definition of the Module. The file has to return a Module object
@return the module interface
]]
function ModuleRegistry:LoadModule(file)
    local mod = require(file)
    if self.modules[mod.name] ~= nil then
        warning("Overwriting module " .. mod.name .. " by loading file " .. file)
        self.modules[mod.name]:DeInit()
    end
    mod:Init()
    self.modules[mod.name] = mod
    information("Loaded Module: " .. mod.name .. " [" .. file .. "]")
    return mod.content
end

local function CreateRegistry()
    if s_moduleRegistry ~= nil then return s_moduleRegistry end
    
    local obj = setmetatable({}, ModuleRegistry)
    obj.__index = obj
    
    obj.modules = {}
    
    return obj
end

s_moduleRegistry = CreateRegistry()

local m = setmetatable({}, {__index = s_moduleRegistry})
m.Module = Module
return m