local pairs = pairs
local ipairs = ipairs
local setmetatable = setmetatable
local assert = assert
local type = type
local tostring = tostring
local min = math.min
local debug = debug

local DirectTransition = {} -- Placeholder table

-- internal utility function
local function ancestors(state)
    if not state.machine then
        return {state}
    end

    local res = {}
    local parent = state
    while parent do
        res[#res+1] = parent
        parent = parent.machine.parent
    end
    
    return res
end

--[[
A Basic state
You can inherit from this class to implement OnEnter and OnExit actions
]]
local State = {}
State.__index = State

--[[
-- Creates a new state
@param name the name of the state
@param submachine the submachine of the state. The state assumes ownership of the submachine!
]]
function State:new(name, submachine)
    local obj = setmetatable({}, self)
    obj.__index = obj
    
    obj.name = name
    obj.submachine = submachine
    if submachine then
        submachine.parent = obj
    end
    return obj
end

function State:SetSubmachine(sm)
    self.submachine = sm
    if sm then
        sm.parent = self
    end
end

function State:OnEnter()
end

function State:OnExit()
end

function State:ToDot()
    local parent = nil
    if self.machine then
        parent = self.machine.parent
    end
    if parent then
        return tostring(parent) .. "_" .. self.name
    else
        return self.name
    end
end

function State:__tostring()
    local parent = nil
    if self.machine then
        parent = self.machine.parent
    end
    if parent then
        return tostring(parent) .. "." .. self.name
    else
        return self.name
    end
end

-----------------------------------------

--[[
A finite state machine (FSM)
]]
local StateMachine = {}
StateMachine.__index = StateMachine

--[[
Creates a new StateMachine
@param states a table consisting of all states in the StateMachine. The StateMachine assumes ownership of all states !
@param start the starting state
@param tt the transition table
            each transition is constructed as follows: {state:from, string:event, state:to, [function:action], [function:condition]}
]]
function StateMachine:new(states, start, tt)
    local obj = setmetatable({}, self)
    obj.__index = obj
    
    obj.states = {}
    obj.transitions = {}
    obj.start = start
    
    for _, v in pairs(states) do
        obj:AddState(v)
    end
    
    for _, v in pairs(tt) do
        obj:AddTransition(v)
    end
    
    return obj
end

--[[
Starts the StateMachine
Will enter the starting state, execute the OnEnter function of the starting state
and the enter all substates of the starting state
@param start the starting state (may be a state of a submachine or nil to use the default start state)
]]
function StateMachine:Start(start, vararg)
    vararg = vararg or {}
    self.last = nil
    
    -- check if start is in current fsm
    local s = nil
    if start and self.states[start.name] == start then
        s = self.states[start.name]
    else
        s = self.start
    end
    
    -- enter start state
    self.current = s
    s:OnEnter(table.unpack(vararg))
    
    -- enter sub machines
    local sub = self.current.submachine
    if sub then
        sub:Start(start, vararg)
    end
    
    -- fire direct transitions
    self:Fire(DirectTransition)
end

--[[
Stops the StateMachine
Will stop all submachines an then execute the OnExit function of the current state
]]
function StateMachine:Stop()
    local sub = self.current.submachine
    if sub then
        sub:Stop()
    end
    self.current:OnExit()
    self.current = nil
end

--[[
Adds a state to the StateMachine
@param s the state to add. The StateMachine assumes ownership of the State!
]]
function StateMachine:AddState(state)
    assert(type(state) == "table")
    assert(type(state.name) == "string", "state.name is nil or not a string")
    assert(state.machine == nil, "state already belongs to a statemachine")
    assert(self.states[state.name] == nil, "there is already a state with the same name")
    
    self.states[state.name] = state
    state.machine = self
end


--[[
Adds a transition to the StateMachine
@param transition the transition to add
          format is {state:from, string:event, state:to, [function:action], [function:condition]}
          
'from' state must be directly owned by this StateMachine
'to' state must be owned by this StateMachine or one of its descendants
]]
function StateMachine:AddTransition(transition)
    local from = transition[1]
    local event = transition[2]
    local to = transition[3]
    local action = transition.action or transition[4]
    local condition = transition.condition or transition[5]
    
    assert(from, "from state is nil")
    assert(type(event) == "string" or event == nil)
    assert(self.states[from.name] == from, "from state doesnt (directly) belong to this statemachine")
    --assert(self:OwnsState(to), "to state doesnt belong to this statemachine")
    
    if event == nil then
        event = DirectTransition
    end
    
    local transTable = self.transitions[from] or {}
    local list = transTable[event] or {}
    
    if not condition then
        for _, trans in ipairs(transTable) do
            assert(trans.condition, "There must only be one conditionless transition per event")
        end
    end
    
    table.insert(list, {to = to, action = action, condition = condition})

    self.transitions[from] = transTable
    transTable[event] = list
end

--[[
Returns true if the given state is owned by this StateMachine or by one of its children
]]
function StateMachine:OwnsState(state)
    local parents = ancestors(state)
    for _, state in ipairs(parents) do
        if state.machine == self then
            return true
        end
    end
    return false
end

function StateMachine:FireDirect()
    return self:Fire(DirectTransition)
end

--[[
Fires the event
@param event the name of the event to fire
@param ... parameters for the action of the event
]]
function StateMachine:Fire(event, ...)
    assert(type(event) == "string" or (type(event) == "table" and event == DirectTransition), "event must be a string")
    
    -- check if event is handled by submachine
    local sub = self.current.submachine
    if sub and event ~= DirectTransition then
        if sub:Fire(event, ...) then
            return true
        end
    end
    
    local transTable = self.transitions[self.current]
    if not transTable then
        return false
    end
    
    local transitions = transTable[event]
    if not transitions then return false end
    
    local elseTrans, toRun
    for _, trans in ipairs(transitions) do
        if not trans.condition then
            elseTrans = trans
        elseif trans.condition() then
            toRun = trans
            break
        end
    end
    
    if not toRun then
        if not elseTrans then
            return false
        end
    
        toRun = elseTrans
    end
    
    -- find common ancestor
    local new = toRun.to
    
    local aa = ancestors(self.current)
    local ab = ancestors(new)
    local a = nil
    for i = min(#aa, #ab), 1, -1 do
        if aa[i] ~= ab[i] then
            break
        end
        a = aa[i]
    end
    
    -- exit all states up to the common ancestor
    local toexit = a or aa[#aa]
    toexit.machine:Stop()
    
    -- execute action
    local vararg = {}
    if toRun.action then
        vararg = {toRun.action(self, ...)}
    else
        vararg = {...}
    end
    
    -- enter all states starting from common ancestor
    local toenter = a or ab[#ab]
    if self.verbose then
        debug("Entering State " .. tostring(toenter))
    end
    
    toenter.machine.current = toenter
    toenter:OnEnter(table.unpack(vararg))
    
    local sub = toenter.submachine
    if sub then
        sub:Start(new, vararg)
    end
    
    -- recursively fire direct transitions
    toenter.machine:FireDirect() -- TODO: make this actually recursive
    
    return true
end

function StateMachine:ToDot()
    local res = ""
    if not self.parent then
        res = res .. "digraph{\n"
        res = res .. "node [shape=Mrecord]\n"
    end
    for name, state in pairs(self.states) do
        if state.submachine == nil then
            res = res .. state:ToDot() .. "[label=\"" .. name .. "\"];\n"
        else
            res = res .. state:ToDot() .. "[label=\"{<fo>" .. name .. "|<f1> \\<\\<submachine\\>\\>}\"];\n"
            
            res = res .. "subgraph cluster_" .. state:ToDot() .. "{\n"
            res = res .. "label = \"submachine " .. name .. "\";\n"
            res = res .. state:ToDot() .. "__START__" .. " [shape=point];\n"
            res = res .. tostring(state.submachine)
            res = res .. "}\n"
            
            res = res .. state:ToDot() .. " -> " .. state:ToDot() .. "__START__ [style = dotted, arrowhead = none]\n"
            res = res .. state:ToDot() .. "__START__ -> " .. state.submachine.start:ToDot() .. "\n"
        end
    end
    for from, transTable in pairs(self.transitions) do
        for event, list in pairs(transTable) do
            for _, trans in ipairs(list) do
                res = res .. from:ToDot() .. " -> " .. trans.to:ToDot() .. " [label = \"" .. event .. "\"]\n"
            end
        end
    end
    if not self.parent then
        res = res .. "__START__ [shape=point];\n"
        res = res .. "__START__ ->" .. self.start:ToDot() .. "\n"
        res = res .. "}\n"
    end
    return res
end

function StateMachine:__tostring()
    return "StateMachine"
end

return {StateMachine = StateMachine, State = State}