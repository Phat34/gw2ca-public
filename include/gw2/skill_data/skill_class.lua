
---
-- \param skillslot the GW2.SkillSlot the skill is in
-- \param name the name of the skill
-- \param castTime the time it takes to cast the skill in ms
-- \param cooldown the cooldown time of the skill in ms
-- \param isAoe true if the skill is an aoe skill false otherwise
function Skill(skillslot, name, castTime, cooldown, range, isAoe)
  local skill =
  {
    skillslot = skillslot,
    name = name,
    castTime = castTime,
    cooldown = cooldown,
    isOnCooldown = false,
    range = range,
    isAoe = isAoe,
  }
  
    local function OnCooldownStart(skill, cd)
    if skill.skillId == Client:GetControlledCharacter():GetSkillbarSkill(skillslot).skillId then
      self.isOnCooldown = true
      Client:GetTimer():RegisterTrigger(OnCooldownEnd, cd)
    else
      Client:GetControlledCharacter():RegisterSingleshot(ControlledCharacter.OnSkillCooldownStart, OnCooldownStart)
    end
  end

  local function OnCooldownEnd(skill)
    self.isOnCooldown = false
  end
  
  ---
  -- casts the skill
  -- \param target the target to cast the skill on
  -- \param position the position to cast the skill on or nil
  function skill:Cast(target, position)
    if (self.isOnCooldown) then return false end
  
    local cc = Client:GetControlledCharacter()
   
    if (position ~= nil) then
      cc:UseSkillbarSlot(self.skillslot, target, position)
    else
      cc:UseSkillbarSlot(self.skillslot, target)
    end
    
    return true
  end

  return skill
end

return 
{
  Skill = Skill,
}