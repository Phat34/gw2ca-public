

-- Skill(skillslot, name, castTime, cooldown, range, isAoe)

local Staff =
{
  ["Fire"] = {
    ["Weapon1"] = Skill(GW2.SkillSlot.Weapon1, "Fireball", 1000, 0, 1200, false),
    ["Weapon2"] = Skill(GW2.SkillSlot.Weapon2, "Lava Font", 0, 6000, 1200, true),
    ["Weapon3"] = Skill(GW2.SkillSlot.Weapon3, "Flame Burst", 500, 10000, 1200, false),
    ["Weapon4"] = Skill(GW2.SkillSlot.Weapon4, "Burning Retreat", 0, 20000, 0, false),
    ["Weapon5"] = Skill(GW2.SkillSlot.Weapon5, "Meteor Shower", 3750, 30000, 1200, true),
  },
  ["Water"] = {
    ["Weapon1"] = Skill(GW2.SkillSlot.Weapon1, "Water Blast", 750, 0, 1200, false),
    ["Weapon2"] = Skill(GW2.SkillSlot.Weapon2, "Ice Spike", 1000, 4000, 1200, true),
    ["Weapon3"] = Skill(GW2.SkillSlot.Weapon3, "Geyser", 750, 20000, 1200, true),
    ["Weapon4"] = Skill(GW2.SkillSlot.Weapon4, "Frozen Ground", 0, 40000, 1200, true),
    ["Weapon5"] = Skill(GW2.SkillSlot.Weapon5, "Healing Rain", 1500, 45000, 1200, true),
  },
  ["Air"] = {
    ["Weapon1"] = Skill(GW2.SkillSlot.Weapon1, "Chain Lightning", 750, 0, 1200, false),
    ["Weapon2"] = Skill(GW2.SkillSlot.Weapon2, "Lightning Surge", 1250, 10000, 1200, false),
    ["Weapon3"] = Skill(GW2.SkillSlot.Weapon3, "Gust", 250, 30000, 1200, false),
    ["Weapon4"] = Skill(GW2.SkillSlot.Weapon4, "Windborne Speed", 250, 30000, 240, false),
    ["Weapon5"] = Skill(GW2.SkillSlot.Weapon5, "Static Field", 750, 40000, 1200, true),
  },
  ["Earth"] = {
    ["Weapon1"] = Skill(GW2.SkillSlot.Weapon1, "Stoning", 750, 0, 1200, false),
    ["Weapon2"] = Skill(GW2.SkillSlot.Weapon2, "Eruption", 1250, 6000, 1200, true),
    ["Weapon3"] = Skill(GW2.SkillSlot.Weapon3, "Magnetic Aura", 0, 30000, 0, false),
    ["Weapon4"] = Skill(GW2.SkillSlot.Weapon4, "Unsteady Ground", 250, 30000, 1200, true),
    ["Weapon5"] = Skill(GW2.SkillSlot.Weapon5, "Shock Wave", 750, 30000, 1200, false),
  }, 
}

local Dagger =
{
  ["Fire"] = {
    ["Weapon1"] = Skill(GW2.SkillSlot.Weapon1, "Dragons Claw", 500, 0, 400, false),
    ["Weapon2"] = Skill(GW2.SkillSlot.Weapon2, "Drakes Breath",2500, 5000, 400, false),
    ["Weapon3"] = Skill(GW2.SkillSlot.Weapon3, "Burning Speed", 750, 15000, 600, false),
    ["Weapon4"] = Skill(GW2.SkillSlot.Weapon4, "Ring of Fire", 250, 15000, 0, false),
    ["Weapon5"] = Skill(GW2.SkillSlot.Weapon5, "Fire Grab", 750, 45000, 300, false),
  },
  ["Water"] = {
    ["Weapon1"] = Skill(GW2.SkillSlot.Weapon1, "Vapor Blade", 500, 0, 600, false),
    ["Weapon2"] = Skill(GW2.SkillSlot.Weapon2, "Cone of Cold", 2250, 10000, 400, false),
    ["Weapon3"] = Skill(GW2.SkillSlot.Weapon3, "Frozen Burst", 250, 15000, 240, false),
    ["Weapon4"] = Skill(GW2.SkillSlot.Weapon4, "Frost Aura", 0, 40000, 0, false),
    ["Weapon5"] = Skill(GW2.SkillSlot.Weapon5, "Cleansing  Wave", 750, 40000, 240, false),
  },
  ["Air"] = {
    ["Weapon1"] = Skill(GW2.SkillSlot.Weapon1, "Lightning Whip", 500, 0, 300, false),
    ["Weapon2"] = Skill(GW2.SkillSlot.Weapon2, "Lightning Touch", 750, 10000, 300, false),
    ["Weapon3"] = Skill(GW2.SkillSlot.Weapon3, "Shocking Aura", 0, 25000, 0, false),
    ["Weapon4"] = Skill(GW2.SkillSlot.Weapon4, "Ride the Lightning", 0, 40000, 1200, false),
    ["Weapon5"] = Skill(GW2.SkillSlot.Weapon5, "Updraft", 0, 40000, 180, false),
  },
  ["Earth"] = {
    ["Weapon1"] = Skill(GW2.SkillSlot.Weapon1, "Impale", 750, 0, 300, false),
    ["Weapon2"] = Skill(GW2.SkillSlot.Weapon2, "Ring of Earth", 750, 6000, 240, false),
    ["Weapon3"] = Skill(GW2.SkillSlot.Weapon3, "Magnetic Grasp", 500, 12000, 900, false),
    ["Weapon4"] = Skill(GW2.SkillSlot.Weapon4, "Earthquake", 750, 45000, 240, false),
    ["Weapon5"] = Skill(GW2.SkillSlot.Weapon5, "Churning Earth", 3250, 30000, 3600, false),
  }, 
}


local Scepter =
{
  ["Fire"] = {
    ["Weapon1"] = Skill(GW2.SkillSlot.Weapon1, "Flamestrike", 1250, 0, 900, false),
    ["Weapon2"] = Skill(GW2.SkillSlot.Weapon2, "Dragons Tooth", 1000, 6000, 900, true),
    ["Weapon3"] = Skill(GW2.SkillSlot.Weapon3, "Phoenix", 250, 20000, 900, true),    
  },
  ["Water"] = {
    ["Weapon1"] = Skill(GW2.SkillSlot.Weapon1, "Ice Shards", 750, 0, 900, false),
    ["Weapon2"] = Skill(GW2.SkillSlot.Weapon2, "Shatterstone", 1000, 2000, 900, true),
    ["Weapon3"] = Skill(GW2.SkillSlot.Weapon3, "trident", 500, 20000, 900, true),   
  },
  ["Air"] = {
    ["Weapon1"] = Skill(GW2.SkillSlot.Weapon1, "Arc Lightning", 3500, 0, 900, false),
    ["Weapon2"] = Skill(GW2.SkillSlot.Weapon2, "Lightning Strike", 0, 5000, 900, false),
    ["Weapon3"] = Skill(GW2.SkillSlot.Weapon3, "Blinding Flash", 0, 10000, 900, false),   
  },
  ["Earth"] = {
    ["Weapon1"] = Skill(GW2.SkillSlot.Weapon1, "Stone Shards", 1500, 0, 900, false),
    ["Weapon2"] = Skill(GW2.SkillSlot.Weapon2, "Rock Barrier", 1000, 15000, 900, false),
    ["Weapon3"] = Skill(GW2.SkillSlot.Weapon3, "Dust Devil", 250, 15000, 900, false),    
  }, 
}

local Focus =
{
  ["Fire"] = {
    ["Weapon4"] = Skill(GW2.SkillSlot.Weapon1, "Flamewall", 750, 20000, 900, true),
    ["Weapon5"] = Skill(GW2.SkillSlot.Weapon2, "Fire Shield", 0, 40000, 0, false),
  },
  ["Water"] = {
    ["Weapon4"] = Skill(GW2.SkillSlot.Weapon1, "Freezing Gust", 250, 2500, 900, false),
    ["Weapon5"] = Skill(GW2.SkillSlot.Weapon2, "Comet", 7500, 25000, 900, false),   
  },
  ["Air"] = {
    ["Weapon4"] = Skill(GW2.SkillSlot.Weapon1, "Swirling Winds", 500, 30000, 400, false),
    ["Weapon5"] = Skill(GW2.SkillSlot.Weapon2, "Gale", 750, 50000, 900, false),   
  },
  ["Earth"] = {
    ["Weapon4"] = Skill(GW2.SkillSlot.Weapon1, "Magnetic Wave", 0, 25000, 300, false),
    ["Weapon5"] = Skill(GW2.SkillSlot.Weapon2, "Obsidian Flesh", 0, 50000, 0, false),   
  }, 
}

local WeaponSkills =
{
  --[GW2.WeaponType.Sword] = Sword,
  --[GW2.WeaponType.Hammer] = Hammer,
  --[GW2.WeaponType.Longbow] = Longbow,
  --[GW2.WeaponType.Shortbow] = Shortbow,
  --[GW2.WeaponType.Axe] = Axe,
  [GW2.WeaponType.Dagger] = Dagger,
  --[GW2.WeaponType.Greatsword] = Greatsword,
  --[GW2.WeaponType.Mace] = Mace,
  --[GW2.WeaponType.Pistol] = Pistol,
  --[GW2.WeaponType.Polearm] = Polearm,
 -- [GW2.WeaponType.Rifle] = Rifle,
  [GW2.WeaponType.Scepter] = Scepter,
  [GW2.WeaponType.Staff] = Staff,
  [GW2.WeaponType.Focus] = Focus,
  --[GW2.WeaponType.Torch] = Torch,  
  --[GW2.WeaponType.Warhorn] = Warhorn,
  --[GW2.WeaponType.Shield] = Shield,  
}

return 
{
  WeaponSkills = WeaponSkills,
}
