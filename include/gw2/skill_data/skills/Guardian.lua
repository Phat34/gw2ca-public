

-- Skill(skillslot, name, castTime, cooldown, range, isAoe)
local Greatsword =
{
  ["Weapon1"] = Skill(GW2.SkillSlot.Weapon1, "Strike", 500, 0, 130, false),
  ["Weapon2"] = Skill(GW2.SkillSlot.Weapon2, "Whirling Wrath", 7500, 10000, 600, false),
  ["Weapon3"] = Skill(GW2.SkillSlot.Weapon3, "Leap of Faith", 500, 15000, 600, false),
  ["Weapon4"] = Skill(GW2.SkillSlot.Weapon4, "Symbol of Wrath", 250, 20000, 180, false),
  ["Weapon5"] = Skill(GW2.SkillSlot.Weapon5, "Blinding Blade", 750, 30000, 600, false),
}

local Hammer =
{
  ["Weapon1"] = Skill(GW2.SkillSlot.Weapon1, "Hammer Swing", 500, 0, 130, false),
  ["Weapon2"] = Skill(GW2.SkillSlot.Weapon2, "Mighty Blow", 750, 5000, 300, false),
  ["Weapon3"] = Skill(GW2.SkillSlot.Weapon3, "Zealots Embrace", 1000, 15000, 1200, false),
  ["Weapon4"] = Skill(GW2.SkillSlot.Weapon4, "Banish", 1000, 25000, 130, false),
  ["Weapon5"] = Skill(GW2.SkillSlot.Weapon5, "Ring of Warding", 750, 40000, 180, false),
}

local Staff =
{
  ["Weapon1"] = Skill(GW2.SkillSlot.Weapon1, "Wave of Wrath", 500, 0, 600, false),
  ["Weapon2"] = Skill(GW2.SkillSlot.Weapon2, "Orb Of Light", 500, 3000, 1200, false),
  ["Weapon3"] = Skill(GW2.SkillSlot.Weapon3, "Symbol of Swiftness", 750, 15000, 1200, false),
  ["Weapon4"] = Skill(GW2.SkillSlot.Weapon4, "Empower", 2500, 20000, 600, false),
  ["Weapon5"] = Skill(GW2.SkillSlot.Weapon5, "Line of Warding", 1000, 40000, 1200, true),
}

local Scepter =
{
  ["Weapon1"] = Skill(GW2.SkillSlot.Weapon1, "Orb of Wrath", 250, 0, 1200, false),
  ["Weapon2"] = Skill(GW2.SkillSlot.Weapon2, "Smite", 250, 6000, 1200, false),
  ["Weapon3"] = Skill(GW2.SkillSlot.Weapon3, "Chains of Light", 250, 20000, 9000, false),
}

local Sword =
{
  ["Weapon1"] = Skill(GW2.SkillSlot.Weapon1, "Sword of Wrath", 500, 0, 150, false),
  ["Weapon2"] = Skill(GW2.SkillSlot.Weapon2, "Flashing Blade", 0, 10000, 600, false),
  ["Weapon3"] = Skill(GW2.SkillSlot.Weapon3, "Zealots Defense", 500, 15000, 600, false),
}

local Shield =
{ 
  ["Weapon4"] = Skill(GW2.SkillSlot.Weapon4, "Shield of Judgment", 500, 30000, 600, false),
  ["Weapon5"] = Skill(GW2.SkillSlot.Weapon5, "Shield of Absorption", 1500, 40000, 0, false),
}

local Torch =
{  
  ["Weapon4"] = Skill(GW2.SkillSlot.Weapon4, "Zealots Flame", 0, 15000, 180, false),
  ["Weapon5"] = Skill(GW2.SkillSlot.Weapon5, "Cleansing Flame", 4500, 15000, 400, false),
}


local WeaponSkills =
{
  [GW2.WeaponType.Sword] = Sword,
  [GW2.WeaponType.Hammer] = Hammer,
  --[GW2.WeaponType.Longbow] = Longbow,
  --[GW2.WeaponType.Shortbow] = Shortbow,
  --[GW2.WeaponType.Axe] = Axe,
  --[GW2.WeaponType.Dagger] = Dagger,
  [GW2.WeaponType.Greatsword] = Greatsword,
  --[GW2.WeaponType.Mace] = Mace,
  --[GW2.WeaponType.Pistol] = Pistol,
  --[GW2.WeaponType.Polearm] = Polearm,
  --[GW2.WeaponType.Rifle] = Rifle,
  [GW2.WeaponType.Scepter] = Scepter,
  [GW2.WeaponType.Staff] = Staff,
  --[GW2.WeaponType.Focus] = Focus,
  [GW2.WeaponType.Torch] = Torch,  
  --[GW2.WeaponType.Warhorn] = Warhorn,
  [GW2.WeaponType.Shield] = Shield,  
}

return 
{
  WeaponSkills = WeaponSkills,
}
