
-- Skill(skillslot, name, castTime, cooldown, range, isAoe)
local Greatsword =
{
  ["Weapon1"] = Skill(GW2.SkillSlot.Weapon1, "Greatsword Swing", 500, 0, 130, false),
  ["Weapon2"] = Skill(GW2.SkillSlot.Weapon2, "Hundred Blades", 3500, 8000, 130, false),
  ["Weapon3"] = Skill(GW2.SkillSlot.Weapon3, "Whirlwind Attack", 1000, 10000, 450, false),
  ["Weapon4"] = Skill(GW2.SkillSlot.Weapon4, "Bladetrail", 750, 15000, 900, false),
  ["Weapon5"] = Skill(GW2.SkillSlot.Weapon5, "Rush", 3500, 20000, 1200, false),
}

local Hammer =
{
  ["Weapon1"] = Skill(GW2.SkillSlot.Weapon1, "Hammer Swing", 500, 0, 130, false),
  ["Weapon2"] = Skill(GW2.SkillSlot.Weapon2, "Fierce Blow", 750, 12000, 150, false),
  ["Weapon3"] = Skill(GW2.SkillSlot.Weapon3, "Hammer Shock", 500, 12000, 600, false),
  ["Weapon4"] = Skill(GW2.SkillSlot.Weapon4, "Staggering Blow", 500, 20000, 130, false),
  ["Weapon5"] = Skill(GW2.SkillSlot.Weapon5, "Backbreaker", 1000, 30000, 130, false),
}

local Rifle =
{
  ["Weapon1"] = Skill(GW2.SkillSlot.Weapon1, "Fierce Shot", 750, 0, 1200, false),
  ["Weapon2"] = Skill(GW2.SkillSlot.Weapon2, "Aimed Shot", 250, 10000, 1200, false),
  ["Weapon3"] = Skill(GW2.SkillSlot.Weapon3, "Volley", 2500, 10000, 1200, false),
  ["Weapon4"] = Skill(GW2.SkillSlot.Weapon4, "Brutal Shot", 1000, 15000, 1200, false),
  ["Weapon5"] = Skill(GW2.SkillSlot.Weapon5, "Rifle Butt", 250, 15000, 130, false),
}

local Longbow =
{
  ["Weapon1"] = Skill(GW2.SkillSlot.Weapon1, "Dual Shot", 750, 0, 1000, false),
  ["Weapon2"] = Skill(GW2.SkillSlot.Weapon2, "Fan of Fire", 250, 6000, 1000, false),
  ["Weapon3"] = Skill(GW2.SkillSlot.Weapon3, "Arcing Arrow", 750, 10000, 1000, false),
  ["Weapon4"] = Skill(GW2.SkillSlot.Weapon4, "Smoldering Arrow", 750, 15000, 1000, false),
  ["Weapon5"] = Skill(GW2.SkillSlot.Weapon5, "Pin Down", 750, 25000, 1000, false),
}

local Sword =
{
  ["Weapon1"] = Skill(GW2.SkillSlot.Weapon1, "Sever Artery", 500, 0, 130, false),
  ["Weapon2"] = Skill(GW2.SkillSlot.Weapon2, "Savage Leap", 750, 8000, 600, false),
  ["Weapon3"] = Skill(GW2.SkillSlot.Weapon3, "Final Thrust", 750, 15000, 130, false),
  ["Weapon4"] = Skill(GW2.SkillSlot.Weapon4, "Impale", 500, 15000, 900, false),
  ["Weapon5"] = Skill(GW2.SkillSlot.Weapon5, "Riposte", 2250, 15000, 130, false),
}

local Axe =
{
  ["Weapon1"] = Skill(GW2.SkillSlot.Weapon1, "Chop", 250, 0, 130, false),
  ["Weapon2"] = Skill(GW2.SkillSlot.Weapon2, "Cyclone Axe", 500, 6000, 130, false),
  ["Weapon3"] = Skill(GW2.SkillSlot.Weapon3, "Throw Axe", 250, 10000, 900, false),
  ["Weapon4"] = Skill(GW2.SkillSlot.Weapon4, "Dual Strike", 500, 12000, 130, false),
  ["Weapon5"] = Skill(GW2.SkillSlot.Weapon5, "Whirling Axe", 3500, 15000, 130, false),
}

local Warhorn =
{  
  ["Weapon4"] = Skill(GW2.SkillSlot.Weapon4, "Charge", 500, 15000, 1200, false),
  ["Weapon5"] = Skill(GW2.SkillSlot.Weapon5, "Call to Arms", 500, 20000, 600, false),
}

local Shield =
{  
  ["Weapon4"] = Skill(GW2.SkillSlot.Weapon4, "Shield Bash", 250, 25000, 450, false),
  ["Weapon5"] = Skill(GW2.SkillSlot.Weapon5, "Call to Arms", 3000, 30000, 0, false),
}


local WeaponSkills =
{
  [GW2.WeaponType.Sword] = Sword,
  [GW2.WeaponType.Hammer] = Hammer,
  [GW2.WeaponType.Longbow] = Longbow,
  --[GW2.WeaponType.Shortbow] = Shortbow,
  [GW2.WeaponType.Axe] = Axe,
  --[GW2.WeaponType.Dagger] = Dagger,
  [GW2.WeaponType.Greatsword] = Greatsword,
  --[GW2.WeaponType.Mace] = Mace,
  --[GW2.WeaponType.Pistol] = Pistol,
  --[GW2.WeaponType.Polearm] = Polearm,
  [GW2.WeaponType.Rifle] = Rifle,
  --[GW2.WeaponType.Scepter] = Scepter,
  --[GW2.WeaponType.Staff] = Staff,
  --[GW2.WeaponType.Focus] = Focus,
  --[GW2.WeaponType.Torch] = Torch,  
  [GW2.WeaponType.Warhorn] = Warhorn,
  [GW2.WeaponType.Shield] = Shield,  
}

return 
{
  WeaponSkills = WeaponSkills,
}
